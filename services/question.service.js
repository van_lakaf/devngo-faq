/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseService = require("../core/classes/base.service");
const { sendMail } = require("../core/utils");
const QuestionRepository = require("../repositories/question.repository");
const UserRepository = require("../repositories/user.repository");

class QuestionService extends BaseService {
  constructor() {
    super();
    this.questionRepo = new QuestionRepository();
    this.userRepo = new UserRepository();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async index(req, res) {
    const questions = (await await this.questionRepo.getAllWithResponses()).map(
      (q) => q.dataValues
    );
    res.json({ result: questions });
    return;
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async show(req, res) {
    const { questionId } = req.params;
    const question = (await this.questionRepo.getElementById(+questionId))
      .dataValues;
    res.json({ result: question });
    return;
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async store(req, res) {
    const { userId, question: questionValue } = req.body;
    const newQuestion = (
      await this.questionRepo.addQuestion({
        userId,
        questionValue,
      })
    ).dataValues;
    res.json({ result: newQuestion.id });

    const users = await this.userRepo.getUsersByRoleId(1);

    users.forEach((u) => {
      sendMail(
        u.dataValues.email,
        "Question",
        `Vous avez de nouvelles questions sans réponse.`
      );
    });

    return;
  }
}

module.exports = QuestionService;
