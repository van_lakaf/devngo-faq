/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseService = require("../core/classes/base.service");
const QuestionRepository = require("../repositories/question.repository");
const ResponseRepository = require("../repositories/response.repository");

class ViewService extends BaseService {
  constructor() {
    super();
    this.questionRepo = new QuestionRepository();
    this.responseRepo = new ResponseRepository();
    this.bindAll();
  }
  
  /**
   * @param { Request } req
   * @param { Response } res
   */
  async homeView(req, res) {
    const questions = (
      await await this.questionRepo.getAllAnsweredQuestions()
    ).map((q) => q.dataValues);

    return res.renderView("home", { questions, title: "Questions ayant obtenues des réponses" });
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async addQuestionView(req, res) {
    return res.renderView("add-question", { title: "Poser une question" });
  }
}

module.exports = ViewService;
