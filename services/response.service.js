/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseService = require("../core/classes/base.service");
const { sendMail } = require("../core/utils");
const QuestionRepository = require("../repositories/question.repository");
const ResponseRepository = require("../repositories/response.repository");
const UserRepository = require("../repositories/user.repository");

class ResponseService extends BaseService {
  constructor() {
    super();
    this.questionRepo = new QuestionRepository();
    this.responseRepo = new ResponseRepository();
    this.userRepo = new UserRepository();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async index(req, res) {
    const { questionId } = req.params;
    const question = await this.questionRepo.getElementById(+questionId);
    if (!question) {
      return res.json({
        error: {
          code: "NOT-FOUND-ERROR",
          message: "Question introuvable.",
        },
      });
    }

    const responses = (
      await this.responseRepo.getResponsesByQuestionId(+questionId)
    ).map((q) => q.dataValues);

    res.json({ result: responses });
    return;
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async store(req, res) {
    const { userid: userId } = req.headers;
    const { questionId } = req.params;
    const question = await this.questionRepo.getElementById(+questionId);
    if (!question) {
      return res.json({
        error: {
          code: "NOT-FOUND-ERROR",
          message: "Question introuvable.",
        },
      });
    }

    const { response } = req.body;

    if (response.trim().length === 0) {
      return res.json({
        error: {
          code: "DATA-ERROR",
          message: "Une réponse est requise.",
        },
      });
    }

    const newResponse = (
      await this.responseRepo.addResponse({
        userId: +userId,
        questionId: +questionId,
        responseValue: response,
      })
    ).dataValues;
    res.json({ result: newResponse.id });

    const user = (await this.userRepo.getElementById(question.userId))
      .dataValues;

    sendMail(
      user.email,
      "Réponse",
      `Salut ${user.username}. <br /> Votre question sur notre foire a eu quelques réponses. <a href="http://localhost:${process.env.port}/web#question-${questionId}">Cliquez ici pour visualiser votre question.</a>`
    );
    return;
  }
}

module.exports = ResponseService;
