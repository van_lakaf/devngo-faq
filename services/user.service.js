/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseService = require("../core/classes/base.service");
const emailValidator = require("email-validator");
const { generateKey, sendMail } = require("../core/utils");
const OTPRepository = require("../repositories/otp.repository");
const SessionRepository = require("../repositories/session.repository");
const UserRepository = require("../repositories/user.repository");

class UserService extends BaseService {
  constructor() {
    super();
    this.userRepo = new UserRepository();
    this.otpRepo = new OTPRepository();
    this.sessionRepo = new SessionRepository();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async signUp(req, res) {
    const { username, email } = req.body;

    if (username.length === 0 || email.length === 0) {
      return res.json({
        error: {
          code: "DATA-ERROR",
          message: "Erreur de données.",
        },
      });
    }

    if (!emailValidator.validate(email)) {
      return res.json({
        error: {
          code: "EMAIL-ERROR",
          message: "Email non valide.",
        },
      });
    }

    const find = await this.userRepo.findUser(email);

    if (find) {
      return res.json({
        error: {
          code: "EMAIL-USED-ERROR",
          message: "Email indisponible.",
        },
      });
    }

    const otp = "$" + generateKey(6);
    sendMail(email, "Code de verification", otp, async (err, info) => {
      if (err) {
        return res.json({
          error: {
            code: "EMAIL-ERROR",
            message: "Une erreur s'est produite.",
          },
        });
      }

      const user = (
        await this.userRepo.addElement({
          email,
          username,
          roleId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        })
      ).dataValues;

      await this.otpRepo.addOTP({
        otpValue: otp,
        userId: user.id,
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      res.json({ result: true });
    });
    return;
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async findUser(req, res) {
    const { email } = req.body;

    if (!emailValidator.validate(email)) {
      return res.json({
        error: {
          code: "EMAIL-ERROR",
          message: "Erreur non valide.",
        },
      });
    }

    const findU = await this.userRepo.findUser(email);

    if (!findU) {
      return res.json({
        error: {
          code: "NOT-FOUND-ERROR",
          message: "Utlisateur introuvable.",
        },
      });
    }

    if (req.path.endsWith("admin") && findU.dataValues.roleId !== 1) {
      return res.json({
        error: {
          code: "NOT-AUTH-ERROR",
          message: "Non autorisé.",
        },
      });
    }

    const otp = "$" + generateKey(6);
    sendMail(email, "Code de verification", otp, async (err, info) => {
      if (err) {
        return res.json({
          error: {
            code: "EMAIL-ERROR",
            message: "Une erreur s'est produite.",
          },
        });
      }

      await this.otpRepo.addOTP({
        otpValue: otp,
        userId: findU.dataValues.id,
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      res.json({ result: true });
    });
    return;
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async verifyUserOTP(req, res) {
    const { email, otp } = req.body;

    if (!emailValidator.validate(email) || otp.length === 0) {
      return res.json({
        error: {
          code: "DATA-ERROR",
          message: "Erreur de données.",
        },
      });
    }

    const findU = await this.userRepo.findUser(email);

    if (!findU) {
      return res.json({
        error: {
          code: "EMAIL-ERROR",
          message: "Code non valide.",
        },
      });
    }

    const findOtp = await this.otpRepo.findUserOTP(findU.dataValues.id, otp);

    if (!findOtp) {
      return res.json({
        error: {
          code: "CODE-ERROR",
          message: "Code non valide.",
        },
      });
    }

    const token = generateKey(32);

    if (req.path.endsWith("admin") && findU.dataValues.roleId !== 1) {
      return res.json({
        error: {
          code: "NOT-AUTH-ERROR",
          message: "Non autorisé.",
        },
      });
    }
    res.json({
      result: {
        token,
        userId: findU.dataValues.id,
      },
    });

    await this.otpRepo.setOTP(findOtp.dataValues.id);
    await this.sessionRepo.addSession({ token, userId: findU.dataValues.id });
    return;
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async logOut(req, res) {
    const { token, userid: userId } = req.headers;
    const session = (
      await this.sessionRepo.findSession({
        token,
        userId: +userId,
      })
    ).dataValues;
    await this.sessionRepo.logoutSession(session.id);
    res.json({ result: true });
    return;
  }
}

module.exports = UserService;
