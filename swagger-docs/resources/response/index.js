const { computePaths } = require("../utils");
const { response } = require("./response");

module.exports = computePaths("/questions/:questionId", {
  "/responses": response,
});
