exports.getResponseSchema = {
  OutputSuccess: {
    type: "object",
    properties: {
      result: {
        type: "array",
        description: "Responses list of a question.",
        items: {
          type: "object",
          description: "Response element.",
          properties: {
            id: {
              type: "integer",
              example: 1,
            },
            userId: {
              type: "integer",
              example: 2,
            },
            questionId: {
              type: "integer",
              example: 2,
            },
            responseValue: {
              type: "string",
              example: "Example of question ?",
            },
            createdAt: {
              type: "string",
              format: "date-time",
            },
            updatedAt: {
              type: "string",
              format: "date-time",
            },
          },
        },
      },
    },
  },
  OutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        example: "ERROR",
      },
      message: {
        type: "string",
        example: "Une érreur s'est produite.",
      },
    },
  },
};
