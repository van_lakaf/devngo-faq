exports.addResponseSchema = {
  AddResponseInput: {
    type: "object",
    description: "Represents the expected payload for create response.",
    properties: {
      response: {
        type: "string",
        example: "Example of response",
      },
    },
  },
  AddResponseOutputSuccess: {
    type: "object",
    description:
      "The response success after adding a response. The result is the id of the added response.",
    properties: {
      result: {
        type: "integer",
        example: 2,
      },
    },
  },
  AddResponseOutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        example: "NOT-AUTHORIZED-ERROR",
      },
      message: {
        type: "string",
        example: "Vous n'êtes pas autorisé.",
      },
    },
  },
};
