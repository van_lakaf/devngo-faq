const { getResponsesSchema } = require("./get-response");
const { addResponseSchema } = require("./add-response");

module.exports = {
  ...getResponsesSchema,
  ...addResponseSchema,
};
