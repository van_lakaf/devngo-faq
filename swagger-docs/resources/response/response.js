exports.response = {
  get: {
    tags: ["Response"],
    description: "Get responses",
    operationId: "getResponses",
    parameters: [
      {
        name: "token",
        in: "header",
        description: "Generated token after logged in",
        required: true,
        type: "string",
        example: "rV5DNNW08s5Ch83bHbMwYZWCiAunz71p",
      },
      {
        name: "userid",
        in: "header",
        description: "Id of current user",
        required: true,
        type: "string",
        example: "1",
      },
      {
        name: "questionId",
        in: "path",
        description: "Id of question to answer",
        required: true,
        type: "string",
        example: "1",
      },
    ],
    responses: {
      200: {
        description: "Response from application after getting responses.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/GetResponseOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/GetResponseOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
  post: {
    tags: ["Response"],
    description: "Add response",
    operationId: "addResponse",
    parameters: [
      {
        name: "token",
        in: "header",
        description: "Generated token after logged in",
        required: true,
        type: "string",
        example: "rV5DNNW08s5Ch83bHbMwYZWCiAunz71p",
      },
      {
        name: "userid",
        in: "header",
        description: "Id of current user",
        required: true,
        type: "string",
        example: "1",
      },
      {
        name: "questionId",
        in: "path",
        description: "Id of question to answer",
        required: true,
        type: "string",
        example: "1",
      },
    ],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/AddResponseInput",
          },
        },
      },
    },
    responses: {
      200: {
        description: "Response from application after adding response.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/AddResponseOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/AddResponseOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
