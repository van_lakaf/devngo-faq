/**
 * @typedef {{ [key: string]: any }} OutputType
 *
 * @param { string } resource
 * @param { OutputType } paths
 * @returns { OutputType }
 */
exports.computePaths = (resource, paths) => {
  /**
   * @type { OutputType }
   */
  const outputData = {};
  for (const key in paths) {
    outputData[`${resource}${key}`] = paths[key];
  }
  return outputData;
};
