exports.addQuestionSchema = {
  AddQuestionInput: {
    type: "object",
    description: "Represents the expected payload for create question.",
    properties: {
      question: {
        type: "string",
        example: "Example of question ?",
      },
      userId: {
        type: "integer",
        example: 1,
      },
    },
  },
  AddQuestionOutputSuccess: {
    type: "object",
    description:
      "The response success after adding a question. The result is the id of the added question.",
    properties: {
      result: {
        type: "integer",
        example: 2,
      },
    },
  },
  AddQuestionOutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        example: "ERROR",
      },
      message: {
        type: "string",
        example: "Une érreur s'est produite.",
      },
    },
  },
};
