const { getQuestionsSchema } = require("./get-question");
const { addQuestionSchema } = require("./add-question");

module.exports = {
  ...getQuestionsSchema,
  ...addQuestionSchema,
};
