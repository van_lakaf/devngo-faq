exports.getQuestionsSchema = {
  GetQuestionsOutputSuccess: {
    type: "object",
    properties: {
      result: {
        type: "array",
        description: "Contains a questions list with responses.",
        items: {
          type: "object",
          description: "Question element with its responses.",
          properties: {
            id: {
              type: "integer",
              example: 1,
            },
            userId: {
              type: "integer",
              example: 2,
            },
            questionValue: {
              type: "string",
              example: "Example of question ?",
            },
            createdAt: {
              type: "string",
              format: "date-time",
            },
            updatedAt: {
              type: "string",
              format: "date-time",
            },
            Responses: {
              type: "array",
              description: "Responses list of a question.",
              items: {
                type: "object",
                description: "Response element.",
                properties: {
                  id: {
                    type: "integer",
                    example: 1,
                  },
                  userId: {
                    type: "integer",
                    example: 2,
                  },
                  questionId: {
                    type: "integer",
                    example: 2,
                  },
                  responseValue: {
                    type: "string",
                    example: "Example of question ?",
                  },
                  createdAt: {
                    type: "string",
                    format: "date-time",
                  },
                  updatedAt: {
                    type: "string",
                    format: "date-time",
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  GetQuestionsOutputFailure: {
    type: "object",
    properties: {
      error: {
        type: "object",
        description: "This object can be returned when request fails.",
        properties: {
          code: {
            type: "string",
            example: "ERROR",
          },
          message: {
            type: "string",
            example: "Une erreur s'est produite.",
          },
        },
      },
    },
  },
  GetQuestionOutputSuccess: {
    type: "object",
    properties: {
      result: {
        type: "object",
        description: "Contains a question information.",
        properties: {
          id: {
            type: "integer",
            example: 1,
          },
          userId: {
            type: "integer",
            example: 2,
          },
          questionValue: {
            type: "string",
            example: "Example of question ?",
          },
          createdAt: {
            type: "string",
            format: "date-time",
          },
          updatedAt: {
            type: "string",
            format: "date-time",
          },
        },
      },
    },
  },
  GetQuestionOutputFailure: {
    type: "object",
    properties: {
      error: {
        type: "object",
        description: "This object can be returned when request fails.",
        properties: {
          code: {
            type: "string",
            example: "NOT-FOUND-ERROR",
          },
          message: {
            type: "string",
            example: "Question indisponible.",
          },
        },
      },
    },
  },
};
