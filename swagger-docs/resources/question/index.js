const { computePaths } = require("../utils");
const { questions, question } = require("./question");

module.exports = computePaths("/questions", {
  "": questions,
  "/:questionId": question,
});
