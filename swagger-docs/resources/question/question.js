exports.questions = {
  get: {
    tags: ["Question"],
    description: "Get questions",
    operationId: "getQuestions",
    parameters: [
      {
        name: "token",
        in: "header",
        description: "Generated token after logged in",
        required: true,
        type: "string",
        example: "rV5DNNW08s5Ch83bHbMwYZWCiAunz71p",
      },
      {
        name: "userid",
        in: "header",
        description: "Id of current user",
        required: true,
        type: "string",
        example: "1",
      },
    ],
    responses: {
      200: {
        description: "Response from application after getting questions.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/GetQuestionsOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/GetQuestionsOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
  post: {
    tags: ["Question"],
    description: "Add question",
    operationId: "addQuestion",
    parameters: [
      {
        name: "token",
        in: "header",
        description: "Generated token after logged in",
        required: true,
        type: "string",
        example: "rV5DNNW08s5Ch83bHbMwYZWCiAunz71p",
      },
      {
        name: "userid",
        in: "header",
        description: "Id of current user",
        required: true,
        type: "string",
        example: "1",
      },
    ],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/AddQuestionInput",
          },
        },
      },
    },
    responses: {
      200: {
        description: "Response from application after adding question.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/AddQuestionOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/AddQuestionOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};

exports.question = {
  get: {
    tags: ["Question"],
    description: "Get question",
    operationId: "getQuestion",
    parameters: [
      {
        name: "token",
        in: "header",
        description: "Generated token after logged in",
        required: true,
        type: "string",
        example: "rV5DNNW08s5Ch83bHbMwYZWCiAunz71p",
      },
      {
        name: "userid",
        in: "header",
        description: "Id of current user",
        required: true,
        type: "string",
        example: "1",
      },
      {
        name: "questionId",
        in: "path",
        description: "Id of question",
        required: true,
        type: "string",
        example: "2",
      },
    ],
    responses: {
      200: {
        description: "Response from application after getting a question.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/GetQuestionOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/GetQuestionOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  }
}
