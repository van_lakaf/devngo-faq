exports.signUp = {
  post: {
    tags: ["Auth"],
    description: "Sign up",
    operationId: "signUp",
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/SignUpInput",
          },
        },
      },
    },
    responses: {
      200: {
        description: "Response from application after signing up.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/SignUpOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/SignUpOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
