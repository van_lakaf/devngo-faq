exports.loginFirstStepAdmin = {
  post: {
    tags: ["Auth"],
    description: "Login first step as admin",
    operationId: "loginFirstStepAdmin",
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/LoginFirstStepAdmin",
          },
        },
      },
    },
    responses: {
      200: {
        description:
          "Response from application after logging in at first step as admin.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/LoginFirstStepAdminOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/LoginFirstStepAdminOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
