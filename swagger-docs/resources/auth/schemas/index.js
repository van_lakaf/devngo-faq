const { logOutSchema } = require("./log-out");
const { loginFirstStepSchema } = require("./login-first-step");
const { loginFirstStepAdminSchema } = require("./login-first-step-admin");
const { loginSecondStepSchema } = require("./login-second-step");
const {
  loginSecondStepAdminAdminSchema,
} = require("./login-second-step-admin");
const { signUpSchema } = require("./sign-up");

module.exports = {
  ...logOutSchema,
  ...loginFirstStepSchema,
  ...loginFirstStepAdminSchema,
  ...loginSecondStepSchema,
  ...loginSecondStepAdminAdminSchema,
  ...signUpSchema,
};
