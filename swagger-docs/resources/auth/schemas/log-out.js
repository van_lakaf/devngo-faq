exports.logOutSchema = {
  LogOutOutputSuccess: {
    type: "object",
    description: "",
    properties: {
      result: {
        type: "boolean",
        example: true,
      },
    },
  },
  LogOutOutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        example: "ERROR",
      },
      message: {
        type: "string",
        example: "Une érreur s'est produite.",
      },
    },
  },
};
