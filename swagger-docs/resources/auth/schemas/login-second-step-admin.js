exports.loginSecondStepAdminAdminSchema = {
  LoginSecondStepAdminInput: {
    type: "object",
    description: "The payload for login as admin at the second step.",
    properties: {
      email: {
        type: "string",
        description: "Represents the email of user.",
        example: "user@domain.ext",
      },
      otp: {
        type: "string",
        description: "Represents the value of the one time password sent to the user email after login first step.",
        example: "user@domain.ext",
      },
    },
  },
  LoginSecondStepAdminOutputSuccess: {
    type: "object",
    description: "",
    properties: {
      result: {
        type: "object",
        description: "Object that contains the token and the id of logged user.",
        properties: {
          userId: {
            type: "integer",
            description: "Represents the user id.",
            example: 2
          },
          token: {
            type: "string",
            description: "Represents the generated token of user for his navigation.",
            example: "rV5DNNW08s5Ch83bHbMwYZWCiAunz71p"
          },
        }
      },
    },
  },
  LoginSecondStepAdminOutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        description: "The possible values are EMAIL-ERROR, NOT-FOUND-ERROR and NOT-AUTH-ERROR.",
        example: "NOT-FOUND-ERROR",
      },
      message: {
        type: "string",
        example: "Utilisateur introuvable.",
      },
    },
  },
};
