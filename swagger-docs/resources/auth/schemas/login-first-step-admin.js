exports.loginFirstStepAdminSchema = {
  LoginFirstStepAdminInput: {
    type: "object",
    description: "The payload for login ad admin at the first step.",
    properties: {
      email: {
        type: "string",
        description: "Represents the email of user.",
        example: "user@domain.ext",
      }
    },
  },
  LoginFirstStepAdminOutputSuccess: {
    type: "object",
    description: "",
    properties: {
      result: {
        type: "boolean",
        description: "In case of success, true is ever returned. Another case return an OutputFailure schema.",
        example: true,
      },
    },
  },
  LoginFirstStepAdminOutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        description: "The possible values are EMAIL-ERROR, NOT-FOUND-ERROR and NOT-AUTH-ERROR.",
        example: "NOT-FOUND-ERROR",
      },
      message: {
        type: "string",
        example: "Utilisateur introuvable.",
      },
    },
  },
};
