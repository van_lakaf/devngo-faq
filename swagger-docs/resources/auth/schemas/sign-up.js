exports.signUpSchema = {
  SignUpInput: {
    type: "object",
    description: "The payload for sign up.",
    properties: {
      email: {
        type: "string",
        description: "Represents the email of user.",
        example: "user@domain.ext",
      },
      username: {
        type: "string",
        description:
          "Represents the full name of user, like described on the example.",
        example: "Admin ADMIN",
      },
    },
  },
  SignUpOutputSuccess: {
    type: "object",
    description: "",
    properties: {
      result: {
        type: "boolean",
        description:
          "In case of success, true is ever returned. Another case return an OutputFailure schema.",
        example: true,
      },
    },
  },
  SignUpOutputFailure: {
    type: "object",
    description: "The response failure after submitting request.",
    properties: {
      code: {
        type: "string",
        description: "The possible values are DATA-ERROR, EMAIL-ERROR and EMAIL-USED-ERROR.",
        example: "EMAIL-USED-ERROR",
      },
      message: {
        type: "string",
        example: "Email indisponible.",
      },
    },
  },
};
