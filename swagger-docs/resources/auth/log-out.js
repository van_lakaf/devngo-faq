exports.logOut = {
  post: {
    tags: ["Auth"],
    description: "Log out",
    operationId: "logOut",
    responses: {
      200: {
        description: "Response from application after logging out.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/LogOutOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/LogOutOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
