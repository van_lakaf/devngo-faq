exports.loginFirstStep = {
  post: {
    tags: ["Auth"],
    description: "Login first step",
    operationId: "loginFirstStep",
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/LoginFirstStepInput",
          },
        },
      },
    },
    responses: {
      200: {
        description:
          "Response from application after logging in at first step.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/LoginFirstStepOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/LoginFirstStepOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
