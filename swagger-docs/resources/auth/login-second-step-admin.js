exports.loginSecondStepAdmin = {
  post: {
    tags: ["Auth"],
    description: "Login second step admin",
    operationId: "loginSecondStepAdmin",
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/LoginSecondStepAdminInput",
          },
        },
      },
    },
    responses: {
      200: {
        description:
          "Response from application after logging in ad admin at second step.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/LoginSecondStepAdminOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/LoginSecondStepAdminOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
