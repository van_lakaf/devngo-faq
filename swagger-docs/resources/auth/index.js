const { computePaths } = require("../utils");
const { logOut } = require("./log-out");
const { loginFirstStep } = require("./login-first-step");
const { loginFirstStepAdmin } = require("./login-first-step-admin");
const { loginSecondStep } = require("./login-second-step");
const { loginSecondStepAdmin } = require("./login-second-step-admin");
const { signUp } = require("./sign-up");

module.exports = computePaths("/auth", {
  "/login/first-step": loginFirstStep,
  "/login/first-step/admin": loginFirstStepAdmin,
  "/login/second-step": loginSecondStep,
  "/login/second-step/admin": loginSecondStepAdmin,
  "/log-out": logOut,
  "/sign-up": signUp,
});
