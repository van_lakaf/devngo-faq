exports.loginSecondStep = {
  post: {
    tags: ["Auth"],
    description: "Login second step",
    operationId: "loginSecondStep",
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/LoginSecondStepInput",
          },
        },
      },
    },
    responses: {
      200: {
        description:
          "Response from application after logging in at second step.",
        content: {
          "application/json": {
            schema: {
              oneOf: [
                {
                  $ref: "#/components/schemas/LoginSecondStepOutputSuccess",
                },
                {
                  $ref: "#/components/schemas/LoginSecondStepOutputFailure",
                },
              ],
            },
          },
        },
      },
      500: {
        description: "Server error",
      },
    },
  },
};
