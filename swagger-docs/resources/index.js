const fs = require("fs");
const path = require("path");

const readDir = fs
  .readdirSync(path.resolve(__dirname))
  .filter((dir) => !/.js$/.test(dir));
let paths = {};

if (readDir.length !== 0) {
  readDir.forEach((dir) => {
    paths = { ...paths, ...require("./" + dir) };
  });
}

module.exports = {
  paths,
};
