const fs = require("fs");
const path = require("path");

const readDir = fs
  .readdirSync(path.resolve(__dirname, "..", "resources"))
  .filter((dir) => !/.js$/.test(dir));
let schemas = {};

if (readDir.length !== 0) {
  readDir.forEach((dir) => {
    schemas = { ...schemas, ...require("../resources/" + dir + "/schemas") };
  });
}

module.exports = {
  components: {
    schemas,
  },
};
