const basicInfo = require("./basic-info");
const servers = require("./servers");
const components = require("./components");
const tags = require("./tags");

module.exports = {
  ...basicInfo,
  ...servers,
  ...components,
  ...tags,
};
