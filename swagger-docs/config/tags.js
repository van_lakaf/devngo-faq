module.exports = {
  tags: [
    {
      name: "Question",
      description: "Endpoints for question",
    },
    {
      name: "Response",
      description: "endpoints for response",
    },
    {
      name: "Auth",
      description: "Endpoints for authentication",
    },
  ],
};
