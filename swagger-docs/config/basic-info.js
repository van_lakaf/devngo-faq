module.exports = {
    openapi: "3.0.3", // present supported openapi version
    info: {
      title: "DevnGo FAQs API V1",
      description: "This is a documentation about DevnGo FAQs API project",
      version: "1.0.0", // version number
      contact: {
        name: "Vaneck KAFFO", // your name
        email: "kanguevan1@gmail.com", // your email
        url: "",
      },
    },
  };
  