const swaggerConfig = require("./config");
const resources = require("./resources");

module.exports = {
  ...swaggerConfig,
  ...resources,
};
