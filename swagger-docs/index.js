const swaggerUi = require("swagger-ui-express");
const docs = require("./docs");

/**
 * @param { import("express").Application } app
 */
function documentation(app) {
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(docs));
}

module.exports = documentation;
