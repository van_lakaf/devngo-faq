/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseController = require("../core/classes/base.controller");
const ResponseService = require("../services/response.service");

class ResponseController extends BaseController {
  constructor() {
    super();
    this.service = new ResponseService();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async index(req, res) {
    return await this.service.index(req, res); 
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async store(req, res) {
    return await this.service.store(req, res);          
  }
}

module.exports = ResponseController;
