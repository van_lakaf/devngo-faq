/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseController = require("../core/classes/base.controller");
const QuestionService = require("../services/question.service");

class QuestionController extends BaseController {
  constructor() {
    super();
    this.service = new QuestionService();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async index(req, res) {
    return await this.service.index(req, res);
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async show(req, res) {
    return await this.service.show(req, res);
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async store(req, res) {
    return await this.service.store(req, res);
  }
}

module.exports = QuestionController;
