/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseController = require("../core/classes/base.controller");
const UserService = require("../services/user.service");

class UserController extends BaseController {
  constructor() {
    super();
    this.service = new UserService();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async signUp(req, res) {
    return await this.service.signUp(req, res);
  }
  
  /**
   * @param { Request } req
   * @param { Response } res
   */
  async findUser(req, res) {
    return await this.service.findUser(req, res);
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async verifyUserOTP(req, res) {
    return await this.service.verifyUserOTP(req, res);
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async logOut(req, res) {
    return await this.service.logOut(req, res);
  }
}

module.exports = UserController;
