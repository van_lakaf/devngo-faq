/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 */

const BaseController = require("../core/classes/base.controller");
const ViewService = require("../services/view.service");

class ViewController extends BaseController {
  constructor() {
    super();
    this.service = new ViewService();
    this.bindAll();
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async homeView(req, res) {
    return this.service.homeView(req, res);
  }

  /**
   * @param { Request } req
   * @param { Response } res
   */
  async addQuestionView(req, res) {
    return this.service.addQuestionView(req, res);
  }
}

module.exports = ViewController;
