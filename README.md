# Journal du project test DevnGo : `DevnGo FAQs`

## 1 - Installation et démarrage
L'installation du projet se fait en ligne de commande via un terminal ouvert depuis la racine de celui-ci, en utilisant le gestionnaire de dépendances NPM :

    `npm i`

Une fois les dépendances du projet installées, il faut se rassurer de disposer d'un serveur mysql présent en local, étant donné que ce projet utilise MySQL comme SGBD.

Le projet dispose un fichier nommé `.env.example` qui contient l'ensemble des variables d'environnement utilisées dans le projet. Tachez de le renommer en `.env` et renseignez les variables présentes par les vôtres, selon vos configurations. Ce fichier contient les variables suivantes :

    port=3033            <-- Cette variable contiendra le port utilisé pour démarrer l'application
    email_server=        <-- Cette variable contiendra le serveur à utiliser pour l'envoi de mails
    email_user=          <-- Cette variable contiendra l'email à utiliser pour l'envoi de mails
    email_password=      <-- Cette variable contiendra le mot de passe à utiliser pour l'envoi de mails
    db_user=             <-- Cette variable contiendra l'utilisateur qui permettra de communiquer avec la base de données (facultative car déjà requise ailleurs)
    db_password=         <-- Cette variable contiendra le mot de passe de l'utilisateur de la base de données (facultative car déjà requise ailleurs)

Le projet contient également un fichier nommé `config/config.example.json`. Tachez de le renommer en `config.json` car il est utilisé par une dépendance pour les interactions entre l'application et la base de données. Ce fichier contient les propriétés suivantes : 

    {
        "development": {             <-- Représente la configuration d'accès à la base de données en développemnt
            "username": "root",
            "password": null,
            "database": "database_development",
            "host": "127.0.0.1",
            "dialect": "mysql"
        },
        "test": {                    <-- Représente la configuration d'accès à la base de données en test
            "username": "root",
            "password": null,
            "database": "database_test",
            "host": "127.0.0.1",
            "dialect": "mysql"
        },                           
        "production": {              <-- Représente la configuration d'accès à la base de données en production
            "username": "root",
            "password": null,
            "database": "database_production",
            "host": "127.0.0.1",
            "dialect": "mysql"
        }
    }

Pour chaque bloc de clés, nous avons exactement les propriétés : `username`, `password`, `database`, `host`, `dialect`.
Remplacez les valeurs par défaut de ces variables par celles correspondant à votre environnement local, excepté `dialect` (étant donné que le projet fonctionne avec une base de données `MySQL`).

En plus des fichiers de configuration, le projet contient des migrations et des seeders qui permettront d'exécuter les operations de mise à jour de la base de données du projet. Ces elements sont contenus dans les dossiers respectifs `migrations` et `seeders` à la racine du projet.

Pour mettre à jour notre base de données, nous devons :
###### 1 - exécuter les migrations
    `npx sequelize-cli db:migrate`
###### 2 - exécuter les seeders
    `npx sequelize-cli db:seed:all`

La prochaine étape après cette installation est le démarrage du projet : 

    `npm run dev`

## 2 - A propos du projet
Ce projet, nommé DevnGo FAQs consiste en une foire aux questions dans laquelle les utilisateurs pourront :
- voir les questions ayant obtenues de réponses
- poser une question
- repondre aux questions posées (en tant qu'administrateur)
- authentification dans la mesure où l'utilisateur souhaite poser une question ou repondre à une question.

Ce projet a été développé en utilisant essentiellement du `VanillaJS` pour les parties client et admin, et `expressjs`, `mysql2.js`, `sequelize`, `nodemailer`, `ejs` et `swagger-ui` (pour la documentation des API REST).

## 3 - Architecture du projet
Ce projet comporte la structure suivante : 

    `config`               <-- contient la configuration pour la base de données
    `controllers`          <-- contient les controleurs utilisés pour le traitement des requêtes
    `core`                 <-- contient les fonctionnalités utilitaires et les classes de base nécessaires dans le cadre de ce projet
    `middlewares`          <-- contient les filtres de requêtes
    `migrations`           <-- contient les fichiers nécessaire pour le mises à jour de la base de données
    `models`               <-- contient les fichiers nécessaires à l'interface de la base de données au sein de l'application
    `public`               <-- représente le dossier public de l'application
    `repositories`         <-- représente une couche d'abstraction permettra de manipuler les ressources de base de données
    `routes`               <-- contient l'nesemble des endpoints disponibles dans l'application (pour le web et l'api)
    `seeders`              <-- contient les fichiers nécessaires au peuplement de la base de données
    `services`             <-- contient toute la logique metier de notre application
    `views`                <-- contient l'ensemble des vues de notre application
    `web-app`              <-- contient l'ensemble des applications web installées dans le projet
    `.env.example`         <-- fichier d'exemple de configuration des variables d'environnement
    `.gitignore`           <-- fichier de cofiguration de git pour l'exclusion de fichiers et/ou dossiers lors des commits
    `.sequelizerc`         <-- fichier de configuration de notre ORM : `sequelize`
    `package.json`         <-- fichier de base du projet requis pour l'installation des dépendances et l'exécution de scripts via `NPM`
    `README.md`            <-- fichier journal du projet
    `server.js`            <-- fichier principal de l'application
    `swagger.json`         <-- fichier de configuration pour la documentation des APIs
    `web-app.config.json`  <-- fichier de configuration pour linstallation des applciations web.

## 4 - Utilisation de l'application
DevnGo FAQs est accessible après le démarrage via le chemin `/web`.

Si nous démarrons notre application sur le port `3033`, alors nous pouvons la visualiser depuis notre anvigateur via l'adresse [`http://localhost:3033/web`](http://localhost:3033/web).

L'administration de DevnGo FAQs est accessible vie le lien [`http://localhost:3033/admin`](http://localhost:3033/admin).

La documentation des API est accessible depuis [`http://localhost:3033/api-docs`](http://localhost:3033/api-docs).
