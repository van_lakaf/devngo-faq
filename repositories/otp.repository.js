const { BaseRepository } = require("../core/classes/base.repository");

class OTPRepository extends BaseRepository {
  constructor() {
    super("OTP");
    this.bindAll();
  }

  /**
   * @param {{
   *   userId: number;
   *   otpValue: string;
   * }} payload
   */
  async addOTP(payload) {
    return await this.addElement({ ...payload, used: false });
  }

  /**
   * @param { number } userId
   * @param { string } otpValue
   */
  async findUserOTP(userId, otpValue) {
    const values = await this.getMany({
      where: { userId, otpValue, used: false },
    });
    return values[0];
  }

  /**
   * @param { number } id
   */
  async setOTP(id) {
    return await this.setElementById({ used: true }, id);
  }
}

module.exports = OTPRepository;
