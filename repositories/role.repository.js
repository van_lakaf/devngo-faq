const { BaseRepository } = require("../core/classes/base.repository");

class RoleRepository extends BaseRepository {
  constructor() {
    super("Role");
    this.bindAll();
  }
}

module.exports = RoleRepository;
