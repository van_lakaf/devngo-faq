const { BaseRepository } = require("../core/classes/base.repository");

class ResponseRepository extends BaseRepository {
  constructor() {
    super("Response");
    this.bindAll();
  }

  /**
   * @param {{
   *   userId: number;
   *   questionId: number;
   *   responseValue: string;
   * }} payload
   */
  async addResponse(payload) {
    return await this.addElement({
      ...payload,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  /**
   * @param { number } questionId
   */
  async getResponsesByQuestionId(questionId) {
    return await this.getMany({ where: { questionId } });
  }
}

module.exports = ResponseRepository;
