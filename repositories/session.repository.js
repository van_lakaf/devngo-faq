const { BaseRepository } = require("../core/classes/base.repository");

class SessionRepository extends BaseRepository {
  constructor() {
    super("Session");
    this.bindAll();
  }

  /**
   * @param {{
   *   token: string;
   *   userId: number;
   * }} payload
   */
  async addSession(payload) {
    return await this.addElement({
      ...payload,
      loggedIn: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  /**
   * @param {{
   *   token: string;
   *   userId: number;
   * }} param0
   */
  async findSession({ token, userId }) {
    return await this.getOne({
      where: { token, userId, loggedIn: true },
    });
  }

  /**
   * @param { number } id
   */
  async logoutSession(id) {
    return await this.setElementById(
      { loggedIn: false, updatedAt: new Date() },
      id
    );
  }
}

module.exports = SessionRepository;
