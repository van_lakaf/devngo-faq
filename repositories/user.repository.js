const { BaseRepository } = require("../core/classes/base.repository");

class UserRepository extends BaseRepository {
  constructor() {
    super("User");
    this.bindAll();
  }

  /**
   * @param {{
   *   email: string;
   *   username: string;
   * }} payload
   */
  async addUser(payload) {
    return await this.addElement({
      email,
      username,
      roleId: 2, // Visitor
    });
  }

  /**
   * @param { string } email
   */
  async findUser(email) {
    return (await this.getMany({ where: { email } }))[0];
  }

  /**
   * @param { number } roleId
   */
  async getUsersByRoleId(roleId) {
    return await this.getMany({ where: { roleId } });
  }
}

module.exports = UserRepository;
