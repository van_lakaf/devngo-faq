const { BaseRepository } = require("../core/classes/base.repository");

class QuestionRepository extends BaseRepository {
  constructor() {
    super("Question");
    this.bindAll();
  }

  /**
   * @param {{
   *   userId: number;
   *   questionValue: string;
   * }} payload
   */
  async addQuestion(payload) {
    return await this.addElement({
      ...payload,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  /**
   * @param { string } questionValue
   * @param { number } id
   */
  async updateQuestion(questionValue, id) {
    return await this.setElementById(
      { questionValue, updatedAt: new Date() },
      id
    );
  }

  async getAllAnsweredQuestions() {
    const { Response } = this.tables;
    return await this.getMany({
      include: {
        model: Response,
        required: true,
      },
    });
  }

  async getAllWithResponses() {
    const { Response } = this.tables;
    return await this.getMany({
      include: {
        model: Response,
      },
    });
  }
}

module.exports = QuestionRepository;
