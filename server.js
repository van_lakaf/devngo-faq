// We load environment variables
require("dotenv").config();

// We require all dependencies we need
const http = require("http");
const path = require("path");
const express = require("express");

// We try to apply database update such as associations.
try {
  require("./models");
  console.log("Database update successfully applied.");
} catch (err) {
  console.error("Cannot apply associations and other configuration.");
  console.error(err);
}

// We require all utilities functions we need to configure our application
const {
  configureThirdPart,
  configureStaticFolders,
  connectWebApps,
  customRender,
} = require("./core/utils");

// We get all endpoints for web and api rest
const { apiRoutes } = require("./routes/api");
const { webRoutes } = require("./routes/web");

// We require all necessary files for swagger docs
const documentation = require("./swagger-docs");

// We get a port to use to launch application
const port = parseInt(process.env.port, 10) || 3030;

// We create our express application
const app = express();

// We configure our created application
app
  .use(express.urlencoded({ extended: true }))
  .use(express.json())
  .set("view engine", "ejs")
  .set("views", path.resolve(process.cwd(), "views"))
  .use(customRender);

configureStaticFolders(app);
connectWebApps(app);

// We configure swagger documentation
documentation(app)

// We load all endpoints and we configure 404 response
app
  .use("/api", apiRoutes)
  .use("/web", webRoutes)
  .use("/*", (req, res) => {
    res.renderView("404", { title: "Page introuvable" });
  });

// We start our application
http.createServer(app).listen(port, () => {
  console.log(`Server started on port ${port}.`);
});
