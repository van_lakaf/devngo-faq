import store from "./store.js";

export function loadHeader() {
  const header = document.createElement("div");
  header.id = "header-app";
  header.classList.add("container");
  header.innerHTML = `<div class="container">
    <header
      class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom"
    >
      <a
        href="/web"
        class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none"
      >
        <span class="fs-4">DevnGo FAQs</span>
      </a>
  
      <ul class="nav nav-pills">
        <li class="nav-item">
          <a 
            data-page-key="home"
            href="#" 
            class="nav-link active" 
            aria-current="page"
          >Accueil</a>
        </li>
        <li class="nav-item">
          <a 
            data-page-key="add-response"
            href="#" 
            class="nav-link" 
          >Répondre à une question</a>
        </li>
      </ul>
    </header>
  </div>
  `;

  const links = header.querySelectorAll("a[data-page-key]");

  store.addActionListener("setCurrentPage", ({ state }) => {
    links.forEach((link) => {
      link.classList.remove("active");
      link.setAttribute("aria-content", "");
    });
    const currentActiveLink = Array.from(links).filter(
      (item) => item.getAttribute("data-page-key") === state.currentPage
    )[0];
    if (currentActiveLink) {
      currentActiveLink.classList.add("active");
      currentActiveLink.setAttribute("aria-content", "page");
    }
  });

  links.forEach((a) =>
    a.addEventListener("click", (e) => {
      e.preventDefault();
      store.dispatch("setCurrentPage", a.getAttribute("data-page-key"));
    })
  );

  return header;
}

export function loadMain() {
  const main = document.createElement("div");
  main.classList.add("container");
  main.id = "main-app";
  return main;
}

export function loadFooter() {
  const footer = document.createElement("div");
  footer.classList.add("container");
  footer.id = "footer-app";
  footer.innerHTML = `<footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <span class="mb-3 mb-md-0 text-muted">&copy; 2022 DevnGo</span>
    </div>
  </footer>`;

  return footer;
}

export function loadCustomAlert() {
  const customAlert = document.createElement("div");
  customAlert.id = "layer";
  customAlert.className = "hide-layer";
  customAlert.innerHTML = `<div class="alert">
    <h3 class="title"></h3>
    <p></p>
    <div class="html"></div>
    <span class="buttons">
      <button></button>
      <button></button>
    </span>
  </div>`;

  return customAlert;
}

export function loadQuestion(data) {
  return `<div class="list-group w-auto" data-key="question-${data.id}">
    <span class="list-group-item list-group-item-action d-flex gap-3 py-3">
      <div class="d-flex gap-2 w-100 justify-content-between">
        <div>
          <h6 class="mb-0">${data.questionValue}</h6>
          <span 
            style="display: inline-block; margin-top: 9px; margin-bottom: 4px; font-size: 14px;"
          >Réponses</span>
          ${data.Responses.reduce((acc, curr) => {
            acc += loadResponse(curr);
            return acc;
          }, "")}
        </div>
      </div>
    </span>
  </div>
  <div style="width: 100%; margin-top: 15px; height: 0;"></div>`;
}

export function loadResponse(data) {
  return `<p class="mb-0 opacity-75" style="margin-left: 15px;">
    ${data.responseValue}
  </p>`;
}
