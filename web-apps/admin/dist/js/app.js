import store from "./store.js";

import {
  gotoHomePage,
  gotoLoginPage,
  gotoQuestionPage,
  loadSkeleton,
} from "./pages.js";
import { checkUser } from "./features.js";

document.addEventListener("DOMContentLoaded", main);

function main() {
  const router = {
    home: gotoHomePage,
    "add-response": gotoQuestionPage,
    login: gotoLoginPage,
  };

  loadSkeleton();

  store
    .addActionListener("retry-login", () =>
      store.dispatch("setCurrentPage", "login")
    )
    .addActionListener("user-logged-in", () =>
      store.dispatch("setCurrentPage", "home")
    )
    .addActionListener("setCurrentPage", ({ state }) => {
      if (!router[state.currentPage]) {
        return store.dispatch("setCurrentPage", "home");
      }
      router[state.currentPage](state.navigationData);
    });

  if (!checkUser()) {
    store.dispatch("setCurrentPage", "login");
  } else {
    store.dispatch("setCurrentPage", "home");
  }
}
