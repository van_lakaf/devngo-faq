import {
  loadCustomAlert,
  loadFooter,
  loadHeader,
  loadMain,
  loadQuestion,
  loadResponse,
} from "./components.js";
import { addResreponse, submitEmail } from "./features.js";
import store from "./store.js";

const appRoot = document.querySelector("#app-root");

export function loadSkeleton() {
  appRoot.appendChild(loadHeader());
  appRoot.appendChild(loadMain());
  appRoot.appendChild(loadFooter());
  appRoot.appendChild(loadCustomAlert());
}

export function gotoLoginPage() {
  const mainApp = document.getElementById("main-app");
  mainApp.innerHTML = `Patientez...`;
  customAlert({
    title: "Connexion requise",
    html: `<div class="container" id="add-question">
    <input 
      class="form-control w-100"
      id="email" 
      placeholder="Votre email..."
    />
    <div style="width: 100%; margin-top: 20px; height: 0;"></div>
    <button 
      class="w-100 btn btn-primary btn-lg" 
      type="button" 
      id="submit-email"
    >S'identifier</button>
  </div>`,
    btns: [],
  });

  document
    .getElementById("submit-email")
    .addEventListener("click", submitEmail);
}

export function gotoHomePage() {
  const mainApp = document.getElementById("main-app");
  mainApp.innerHTML = `<p align="center">Patientez un instant...</p>`;

  const { userId, token } = sessionStorage;
  api
    .get("/api/questions", { headers: { token, userid: userId } })
    .then((data) => {
      if (data.error) {
        mainApp.innerHTML = `<p align="center">${data.error.message}</p>`;
        return store.dispatch("setCurrentPage", "login");
      }

      /** @type { any[] } */
      const result = data.result;
      mainApp.innerHTML = result.reduce((acc, curr) => {
        acc += loadQuestion(curr);
        return acc;
      }, "");

      result.forEach((el) => {
        document
          .querySelector(`[data-key="question-${el.id}"]`)
          .addEventListener("click", () => {
            store
              .dispatch("setNavigationData", { questionId: el.id, data: el })
              .then(() => {
                store.dispatch("setCurrentPage", "add-response");
              });
          });
      });
    })
    .catch((err) => {
      customAlert({
        title: "Erreur",
        message: "Une erreur est survenue lors de la récupération des données.",
        btns: [
          {
            label: "Réessayer",
            callback() {
              gotoHomePage();
            },
          },
        ],
      });
    });
}

export function gotoQuestionPage(props) {
  if (!props) {
    return store.dispatch("setCurrentPage", "home");
  }
  const { questionId = null, data = null } = props;
  if (!questionId || typeof questionId !== "number") {
    return store.dispatch("setCurrentPage", "home");
  }

  const mainApp = document.getElementById("main-app");

  mainApp.innerHTML = `<div class="list-group w-auto" data-key="question-${
    data.id
  }">
    <span class="list-group-item list-group-item-action d-flex gap-3 py-3">
        <div class="d-flex gap-2 w-100 justify-content-between">
        <div>
            <h6 class="mb-0">${data.questionValue}</h6>
            <span 
            style="display: inline-block; margin-top: 9px; margin-bottom: 4px; font-size: 14px;"
            >Réponses</span>
            ${data.Responses.reduce((acc, curr) => {
              acc += loadResponse(curr);
              return acc;
            }, "")}
        </div>
        </div>
    </span>
    </div>
    <div style="width: 100%; margin-top: 15px; height: 0;"></div>
    <div class="row g-3">
        <div class="col-12">
        <textarea 
            class="form-control" 
            rows="6" 
            style="resize: none;" 
            id="response" 
            placeholder="Votre réponse..."
        ></textarea>
        </div>
    </div>
    <div style="width: 100%; margin-top: 30px; height: 0;"></div>
    <button 
        class="w-100 btn btn-primary btn-lg" 
        type="button" 
        id="submit-response"
    >Soumettre</button>`;

  document
    .getElementById("submit-response")
    .addEventListener("click", () =>
      addResreponse(
        questionId,
        document.getElementById("response").value.trim()
      )
    );
}
