import store from "./store.js";

export function validateUser() {
  const otp = document.querySelector("#otp").value.trim();
  const email = document.querySelector("#email").value.trim();

  if (otp.length === 0 || email.length === 0) {
    alert("email ou OTP invalides.");
    return;
  }

  api
    .post("/api/auth/login/second-step/admin", {
      body: { otp, email },
    })
    .then((data) => {
      if (data.error) {
        return customAlert({
          title: "Erreur",
          message: data.error.message,
          btns: [
            {
              label: "Réessayer",
              callback() {
                store.dispatch("retry-login");
              },
            },
          ],
        });
      }

      const { token, userId } = data.result;
      sessionStorage.setItem("userId", userId);
      sessionStorage.setItem("token", token);

      customAlert({
        title: "Alerte",
        message: "Verification terminée",
        btns: ["close"],
      });
      setTimeout(() => {
        store.dispatch("user-logged-in");
      }, 3000);
    })
    .catch((err) => {
      customAlert({
        title: "Erreur",
        message: "Une erreur s'est produite.",
        btns: ["close"],
      });
    });
}

export function submitEmail() {
  const email = document.querySelector("#email").value.trim();

  if (email.length === 0) {
    alert("Les champs email et nom sont requis pour se faire enregistrer.");
    return;
  }

  api
    .post("/api/auth/login/first-step/admin", {
      body: { email },
    })
    .then((data) => {
      if (data.error) {
        return customAlert({
          title: "Erreur",
          message: data.error.message,
          btns: [
            {
              label: "Réessayer",
              callback() {
                store.dispatch("retry-login");
              },
            },
          ],
        });
      }

      customAlert({
        title: "Verification",
        html: `<div class="container" id="add-question">
        <p>Nous vous invitons à rentrer le code qui a été envoyé à votre email.</p>
        <input 
          class="form-control w-100"
          id="email" 
          hidden
          value="${email}"
          disabled
        />
        <input 
          class="form-control w-100"
          id="otp" 
          placeholder="Votre OTP..."
        />
        <div style="width: 100%; margin-top: 20px; height: 0;"></div>
        <button 
          class="w-100 btn btn-primary btn-lg" 
          type="button" 
          id="validate-user"
          onclick="validateUser()"
        >Valider</button>
      </div>`,
        btns: [],
      });

      document
        .getElementById("validate-user")
        .addEventListener("click", validateUser);
    })
    .catch((err) => {
      customAlert({
        title: "Erreur",
        message: "Une erreur s'est produite.",
        btns: ["close"],
      });
    });
}

/**
 * @param { number } questionId
 * @param { string } response
 */
export function addResreponse(questionId, response) {
  if (response.length === 0) {
    return customAlert({
      title: "Erreur",
      message: "Vous devez remplir la zone de texte avec votre réponse.",
      btns: ["close"],
    });
  }

  customAlert({
    title: "Traitement",
    message: "Nous soumettons votre réponse.",
    btns: [],
  });

  api
    .post("/api/questions/" + questionId + "/responses", {
      body: { response },
      headers: { token: sessionStorage.token, userid: sessionStorage.userId },
    })
    .then((data) => {
      if (data.error) {
        if (data.error.code === "NOT-AUTH-ERROR") {
          return customAlert({
            title: "Erreur",
            message: data.error.message,
            btns: [
              {
                label: "S'enregister",
                callback: () => {
                  customAlert({
                    title: "Connexion requise",
                    message:
                      "Vous n'êtes pas connecté. Afin de publier votre réponse, veuillez vous connecter.",
                    btns: [
                      {
                        label: "Se connecter",
                        callback() {
                          store.dispatch("retry-login");
                        },
                      },
                    ],
                  });
                  store.addActionListener("user-logged-in", () =>
                    addResreponse(questionId, response)
                  );
                },
              },
              "close",
            ],
          });
        }
      }

      customAlert({
        title: "Information",
        message: "Votre réponse a été envoyée.",
        btns: [
          {
            label: "Accueil",
            callback() {
              store.dispatch("setCurrentPage", "home");
              customAlert.close();
            },
          },
        ],
      });
    });
}

export function checkUser() {
  return !!sessionStorage.userId && !!sessionStorage.token;
}
