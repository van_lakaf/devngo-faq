const store = new Store({
  store: {
    currentPage: "home",
    navigationData: {}
  },
  mutations: {
    setCurrentPage(state, value) {
      return { ...state, currentPage: value };
    },
    setNavigationData(state, value) {
      return { ...state, navigationData: value };
    }
  },
  actions: {
    setCurrentPage({ commit }, value) {
      return new Promise((resolve, reject) => {
        commit("setCurrentPage", value);
        resolve();
      });
    },
    setNavigationData({ commit }, value) {
      return new Promise((resolve, reject) => {
        commit("setNavigationData", value);
        resolve();
      });
    },
  },
  empty: ["retry-login", "user-logged-in"],
});

export default store;
