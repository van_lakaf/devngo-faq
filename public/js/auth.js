function loadSignUpForm() {
  return `<div class="container" id="add-question">
    <p>Nous aimerions savoir qui nous porte autant d'attention.</p>
    <div style="width: 100%; margin-top: 15px; height: 0;"></div>
    <input 
      class="form-control w-100"
      id="username" 
      type="text"
      placeholder="Votre nom..."
    />
    <div style="width: 100%; margin-top: 10px; height: 0;"></div>
    <input 
      class="form-control w-100"
      id="email" 
      type="email"
      placeholder="Votre email..."
    />
    <div style="width: 100%; margin-top: 20px; height: 0;"></div>
    <button 
      class="w-100 btn btn-primary btn-lg" 
      type="button" 
      id="sign-up"
      onclick="signUp()"
    >S'enregistrer</button>
    <div style="width: 100%; margin-top: 10px; height: 0;"></div>
    <button 
      class="w-100 btn btn-primary btn-lg" 
      type="button" 
      id="load-login-form"
      onclick="loadLoginForm()"
    >S'identifier plutôt</button>
  </div>`;
}

function loadLoginForm() {
  return customAlert({
    title: "Connexion requise",
    html: `<div class="container" id="add-question">
    <input 
      class="form-control w-100"
      id="email" 
      placeholder="Votre email..."
    />
    <div style="width: 100%; margin-top: 20px; height: 0;"></div>
    <button 
      class="w-100 btn btn-primary btn-lg" 
      type="button" 
      id="submit-email"
      onclick="submitEmail()"
    >S'identifier</button>
  </div>`,
    btns: ["close"],
  });
}

function signUp() {
  const username = document.querySelector("#username").value.trim();
  const email = document.querySelector("#email").value.trim();

  if (username.length === 0 || email.length === 0) {
    alert("Les champs email et nom sont requis pour se faire enregistrer.");
    return;
  }

  api
    .post("/api/auth/sign-up", {
      body: { username, email },
    })
    .then((data) => {
      if (data.error) {
        return customAlert({
          title: "Erreur",
          message: data.error.message,
          btns: ["close"],
        });
      }
      customAlert({
        title: "Verification",
        html: `<div class="container" id="add-question">
      <p>Nous vous invitons à rentrer le code qui a été envoyé à votre email.</p>
      <input 
        class="form-control w-100"
        id="email" 
        hidden
        value="${email}"
        disabled
      />
      <input 
        class="form-control w-100"
        id="otp" 
        placeholder="Votre OTP..."
      />
      <div style="width: 100%; margin-top: 20px; height: 0;"></div>
      <button 
        class="w-100 btn btn-primary btn-lg" 
        type="button" 
        id="submit-email"
        onclick="validateUser()"
      >Valider</button>
    </div>`,
        btns: [],
      });
    })
    .catch((err) => {
      customAlert({
        title: "Erreur",
        message: "Une erreur s'est produite.",
        btns: ["close"],
      });
    });
}

function validateUser() {
  const otp = document.querySelector("#otp").value.trim();
  const email = document.querySelector("#email").value.trim();

  if (otp.length === 0 || email.length === 0) {
    alert("email ou OTP invalides.");
    return;
  }

  api
    .post("/api/auth/login/second-step", {
      body: { otp, email },
    })
    .then((data) => {
      if (data.error) {
        return customAlert({
          title: "Erreur",
          message: data.error.message,
          btns: ["close"],
        });
      }

      const { token, userId } = data.result;
      sessionStorage.setItem("userId", userId);
      sessionStorage.setItem("token", token);

      customAlert({
        title: "Alerte",
        message: "Verification terminée",
        btns: [],
      });
      setTimeout(() => {
        store.dispatch("user-logged-in");
      }, 3000)
    })
    .catch((err) => {
      customAlert({
        title: "Erreur",
        message: "Une erreur s'est produite.",
        btns: ["close"],
      });
    });
}

function submitEmail() {
  const email = document.querySelector("#email").value.trim();

  if (email.length === 0) {
    alert("Les champs email et nom sont requis pour se faire enregistrer.");
    return;
  }

  api
    .post("/api/auth/login/first-step", {
      body: { email },
    })
    .then((data) => {
      if (data.error) {
        return customAlert({
          title: "Erreur",
          message: data.error.message,
          btns: ["close"],
        });
      }

      customAlert({
        title: "Verification",
        html: `<div class="container" id="add-question">
      <p>Nous vous invitons à rentrer le code qui a été envoyé à votre email.</p>
      <input 
        class="form-control w-100"
        id="email" 
        hidden
        value="${email}"
        disabled
      />
      <input 
        class="form-control w-100"
        id="otp" 
        placeholder="Votre OTP..."
      />
      <div style="width: 100%; margin-top: 20px; height: 0;"></div>
      <button 
        class="w-100 btn btn-primary btn-lg" 
        type="button" 
        id="submit-email"
        onclick="validateUser()"
      >Valider</button>
    </div>`,
        btns: [],
      });
    })
    .catch((err) => {
      customAlert({
        title: "Erreur",
        message: "Une erreur s'est produite.",
        btns: ["close"],
      });
    });
}

