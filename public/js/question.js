const submitQuestionBtn = document.querySelector("#submit-question");

if (!!submitQuestionBtn) {
  submitQuestionBtn.addEventListener("click", submitQuestion);
}

function submitQuestion() {
  const { token, userId } = sessionStorage;

  if (!token || !userId) {
    store.addActionListener("user-logged-in", () => submit(token, userId), {
      once: true,
    });
    return customAlert({
      title: "Connexion requise",
      html: loadSignUpForm(),
      btns: ["close"],
    });
  }

  submit(token, userId);
}

/**
 * @param { string } token
 * @param { number } userId
 */
function submit(token, userId) {
  const question = document.querySelector("#question").value.trim();

  if (question.length === 0) {
    return customAlert({
      title: "Erreur",
      message: "Vous devez remplir la zone de texte avec votre question.",
      btns: ["close"],
    });
  }

  customAlert({
    title: "Traitement",
    message: "Nous soumettons votre question.",
    btns: [],
  });

  api
    .post("/api/questions", {
      body: { userId, question },
      headers: { token, userid: userId },
    })
    .then((data) => {
      if (data.error) {
        if (data.error.code === "NOT-AUTH-ERROR") {
          return customAlert({
            title: "Erreur",
            message: data.error.message,
            btns: [
              {
                label: "S'enregister",
                callback: () => {
                  customAlert({
                    title: "Connexion requise",
                    html: loadSignUpForm(),
                    btns: ["close"],
                  });
                  store.addActionListener("user-logged-in", () =>
                    submit(sessionStorage.token, sessionStorage.userId)
                  );
                },
              },
            ],
          });
        }
      }

      customAlert({
        title: "Information",
        message: "Votre question a été envoyée.",
        btns: [
          {
            label: "Accueil",
            callback() {
              location.pathname = "/web";
            },
          },
        ],
      });
    });
}
