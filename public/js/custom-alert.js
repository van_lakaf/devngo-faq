/**
 * @typedef { ({ label: string; callback: () => void; } | "close")[] } btnConfig
 */

/**
 * @param {{ title: string; message?: string; html?: string; btns: btnConfig }} config
 */
function customAlert(config) {
  const { title = "Alert", message = "", html = "", btns = [] } = config;

  const closeIndex = btns.findIndex((val) => val === "close");
  if (closeIndex !== -1) {
    btns[closeIndex] = {
      label: "Fermer",
      callback: customAlert.close,
    };
  }
  const alertElement = document.querySelector("#layer .alert");

  alertElement.querySelector(".title").textContent = title;
  alertElement.querySelector("p").textContent = message;
  alertElement.querySelector(".html").innerHTML = html;

  const alertButtons = alertElement.querySelector(".buttons");
  alertButtons.innerHTML = btns
    .map(
      (el, index) => `<button id="alert-button-${index}">${el.label}</button>`
    )
    .join("\n");

  if (btns.length !== 0) {
    btns.forEach((b, index) => {
      alertButtons
        .querySelector("#alert-button-" + index)
        .addEventListener("click", b.callback);
    });
  }

  customAlert.open();
}

customAlert.close = function () {
  document
    .querySelector("#layer")
    .classList.replace("show-layer", "hide-layer");
};

customAlert.open = function () {
  document
    .querySelector("#layer")
    .classList.replace("hide-layer", "show-layer");
};
