document.getElementById("admin-office").addEventListener("click", logout);

/**
 * @param { MouseEvent } e
 */
function logout(e) {
  e.preventDefault();
  api
    .post("/api/admin/logout", {
      headers: {
        userid: localStorage.userId,
      },
    })
    .then((data) => {
      if (data.error) {
        return customAlert({
          title: "Erreur: " + data.error.code,
          message: data.error.message,
          btns: [
            "close",
            {
              label: "Reessayer",
              callback: () => logout(e),
            },
          ],
        });
      }
      localStorage.clear();
      return customAlert({
        title: "Alerte",
        message:
          "Vous avez été déconnecté de l'administration de Dimanche Foot.",
        btns: [
          {
            label: "Aller à l'accueil",
            callback: () => {
              window.location.href = "/";
            },
          },
        ],
      });
    });
}
