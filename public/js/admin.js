const authorization = localStorage.getItem("token");

if (!authorization && location.pathname === "/admin") {
  window.location.pathname = "/admin/auth/login-form";
}
