/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 * @typedef { import("express").NextFunction } NextFunction
 */

const SessionRepository = require("../repositories/session.repository");

const sessionRepo = new SessionRepository();

module.exports = async function (
  /** @type { Request } */ req,
  /** @type { Response } */ res,
  /** @type { NextFunction } */ next
) {
  const { token, userid: userId } = req.headers;
  if (!token || !userId || token === "undefined" || userId === "undefined") {
    return res.json({
      error: {
        code: "NOT-AUTH-ERROR",
        message: "Nous aimerions savoir qui vous êtes.",
      },
    });
  }
  const session = await sessionRepo.findSession({ token, userId: +userId });
  if (!session) {
    return res.json({
      error: {
        code: "NOT-AUTH-ERROR",
        message: "Nous aimerions savoir qui vous êtes.",
      },
    });
  }
  next();
};
