/**
 * @typedef { import("express").Request } Request
 * @typedef { import("../core/utils").Res } Response
 * @typedef { import("express").NextFunction } NextFunction
 */

const UserRepository = require("../repositories/user.repository");

const userRepo = new UserRepository();

module.exports = async function (
  /** @type { Request } */ req,
  /** @type { Response } */ res,
  /** @type { NextFunction } */ next
) {
  const { userid: userId } = req.headers;
  const findU = (await userRepo.getElementById(+userId)).dataValues;

  if (findU.roleId !== 1) {
    return res.json({
      error: {
        code: "ROLE-ERROR",
        message: "Non autorisé.",
      },
    });
  }

  next();
};
