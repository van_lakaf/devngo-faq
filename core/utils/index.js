/**
 * @typedef {{
 *   title: string;
 *   message: string;
 *   code: string;
 *   type: "error" | "info" | "warning"
 *   location: string;
 *   date: string | number;
 *   error?: { code: string; message: string; }
 * }} LogData
 *
 * @typedef { import("express").Response & { renderView: ( view: string; ...rest: [first: Record<string, any> | (err: any) => void, cb: (err: any) => void] ) } } Res
 */

const fs = require("fs");
const express = require("express");
const ejs = require("ejs");
const path = require("path");
const nodemailer = require("nodemailer");

/**
 * @param { express.Application } app
 */
function connectWebApps(app) {
  const config = require("../../web-app.config.json");
  for (const key in config) {
    if (Object.hasOwnProperty.call(config, key)) {
      app.use(`/${key}`, express.static(`web-apps/${key}/dist`));
    }
  }
}

/**
 * @typedef {(view: string; rest: [options?: ejs.Options & { async: true, client?: false }, cb?: (err) => void])} RenderViewFunction
 * @param { express.Request } req
 * @param { express.Response } res
 * @param { express.NextFunction } next
 */
async function customRender(req, res, next) {
  /**
   * @property { RenderViewFunction } renderView
   */
  res.renderView = function (view, ...rest) {
    const headers = { "Content-Type": "text/html" };
    const [a, b] = rest;
    const defaultLayout = "public/index.html";
    const defaultOptions = {
      page: `pages/${view}.page.ejs`,
      view,
      resolveHtmlBloc: (value) => path.resolve(process.cwd(), "views", value),
    };
    let options, cb;
    if (!a) {
      options = defaultOptions;
      cb = undefined;
    }
    if (typeof a === "function") {
      options = defaultOptions;
      cb = a;
    }
    if (typeof a === "object") {
      options = { ...a, ...defaultOptions };
      cb = typeof b === "function" ? b : undefined;
    }
    const layout = path.resolve(
      process.cwd(),
      !options.layout ? defaultLayout : options.layout
    );
    try {
      const template = fs.readFileSync(layout, { encoding: "utf-8" });
      const html = ejs.compile(template)(options);
      res.set(headers);
      res.send(html);
      return;
    } catch (err) {
      if (typeof cb === "function") {
        cb(err);
      }
    }
  };
  next();
}

/**
 * @param { express.Application } app
 * @param { express.Router } routes
 * @param { string } scope
 */
function configureThirdPart(app, routes, scope = "/third-party-api") {
  app.use(
    scope,
    (req, res, next) => {
      res.set({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
      });
      next();
    },
    routes
  );
}

/***
 * @param { LogData } data
 */
async function createLog(data) {
  const keys = Object.keys(data);
  if (keys.length === 0) return;
  const logName = `${data.code}-${data.date}.txt`;
  const content = keys.reduce((output, value, index) => {
    output += (index !== 0 ? "\n" : "") + `${value} : ${data[value]}`;
    return output;
  }, "");

  fs.writeFileSync(path.resolve(process.cwd(), "logs", logName), content, {
    encoding: "utf-8",
  });
}

/**
 * @template T
 * @param { T } obj
 * @returns { T }
 */
function clone(obj) {
  return JSON.parse(JSON.stringify({ obj })).obj;
}

/**
 * @param { express.Application } app
 */
function configureStaticFolders(app) {
  app
    .use("/assets", express.static(process.cwd() + "/public/assets"))
    .use("/css", express.static(process.cwd() + "/public/css"))
    .use("/js", express.static(process.cwd() + "/public/js"))
    .use("/images", express.static(process.cwd() + "/public/images"))
    .use("/videos", express.static(process.cwd() + "/public/videos"))
    .use("/icons", express.static(process.cwd() + "/public/icons"));
}

const numDico = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
const alphaDico = [
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
];

/**
 * @param { number } min
 * @param { number } max
 */
function generateInt(min = 6, max = 64) {
  if (
    typeof min !== "number" ||
    !Number.isInteger(min) ||
    typeof max !== "number" ||
    !Number.isInteger(max) ||
    max <= min
  ) {
    throw new Error("min and max parameters must be integers.");
  }
  return min + Math.floor(Math.random() * (max - min));
}

/**
 * @param { number } length
 */
function generateKey(length) {
  if (!length) {
    return generateKey(generateInt());
  }
  if (typeof length !== "number" || !Number.isInteger(length) || length <= 0) {
    throw new Error("length must be a positive integer.");
  }
  const dico = [...alphaDico, ...numDico];
  let result = "";
  for (var i = 0; i < length; i++) {
    result += dico[Math.floor(Math.random() * (dico.length - 1))];
  }
  return result;
}

/**
 * @param { string | { name: string; address: string; } } receiver
 * @param { string } subject
 * @param { string } content
 * @param { Function | null } cb
 */
function sendMail(receiver, subject, content, cb) {
  const { email_host, email_user, email_password } = process.env;
  const t = nodemailer.createTransport({
    host: email_host,
    port: 465,
    auth: {
      user: email_user,
      pass: email_password,
    },
    "Content-Type": "text/html",
  });

  const opts = {
    from: {
      address: email_user,
      name: "DevnGo Alerts",
    },
    to: receiver,
    subject,
    text: content,
    html: `<div>${content}</div>`,
  };

  t.sendMail(opts, cb);
}

exports.connectWebApps = connectWebApps;
exports.createLog = createLog;
exports.clone = clone;
exports.configureStaticFolders = configureStaticFolders;
exports.configureThirdPart = configureThirdPart;
exports.customRender = customRender;
exports.generateInt = generateInt;
exports.generateKey = generateKey;
exports.sendMail = sendMail;
