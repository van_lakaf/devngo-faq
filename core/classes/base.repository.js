const db = require("../../models");
const Root = require("./root");

class BaseRepository extends Root {
  constructor(modelName) {
    super();
    this.m = db[modelName];
    this.t = db;
  }

  get model() {
    return this.m;
  }

  get tables() {
    return this.t;
  }

  async addElement(payload) {
    return await this.m.create(payload);
  }

  async getElementById(id) {
    return await this.m.findOne({ where: { id } });
  }

  async getOne(clauses) {
    return await this.m.findOne(clauses);
  }

  async getAll() {
    return await this.m.findAll();
  }

  async getManyWithPagination(limit, offset, clauses = {}) {
    return await this.m.findAll({ limit, offset, ...clauses });
  }

  async getMany(clauses) {
    return await this.m.findAll(clauses);
  }

  async setElementById(payload, id) {
    return await this.m.update(payload, { where: { id } });
  }

  async removeElementById(id) {
    return await this.m.destroy({ where: { id } });
  }

  async removeElements(clauses) {
    return await this.m.destroy({ ...clauses });
  }

  async removeAll() {
    return await this.m.destroy({
      truncate: true,
    });
  }
}

exports.BaseRepository = BaseRepository;
