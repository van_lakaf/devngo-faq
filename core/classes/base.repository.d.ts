import { Model } from "sequelize";
import Root from "./root";

export declare class BaseRepository extends Root {
  private m: Model;
  private t: Record<string, Model>;

  constructor(modelName: string);

  get model(): Model;

  get tables(): Record<string, Model>;

  addElement(payload: Record<string, any>): Promise<Record<string, any>>;

  getElementById(id: number): Promise<Record<string, any>>;

  getOne(clauses: Record<string, any>): Promise<Record<string, any>>;

  getAll(): Promise<Record<string, any>[]>;

  getManyWithPagination(
    limit: number,
    offset: number,
    clauses?: Record<string, any>
  );

  getMany(clauses: Record<string, any>): Promise<Record<string, any>[]>;

  setElementById(
    payload: Record<string, any>,
    id: number
  ): Promise<Record<string, any>>;

  removeElementById(id: number): Promise<Record<string, any>>;

  removeElements(clauses: Record<string, any>): Promise<Record<string, any>[]>;

  removeAll(): Promise<any>;
}
