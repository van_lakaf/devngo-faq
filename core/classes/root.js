const { clone, createLog } = require("../utils");

class Root {
  constructor() {
    this.bindAll = this.bindAll.bind(this);

    this.createLog = createLog;
    this.clone = clone;
  }

  bindAll() {
    //Get all properties of object
    const proto = Object.getPrototypeOf(this);
    const props = [...Object.getOwnPropertyNames(proto)];

    //Bind all methods
    for (const prop of props) {
      if (typeof this[prop] === "function") {
        this[prop] = this[prop].bind(this);
      }
    }
  }
}

module.exports = Root;
