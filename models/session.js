"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Session extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     * @param {{ [member: string]: Model; }} models
     */
    static associate(models) {
      const { User } = models;
      User.hasMany(Session, {
        foreignKey: "userId",
      });
      Session.belongsTo(User);
    }
  }
  Session.init(
    {
      userId: DataTypes.INTEGER,
      token: DataTypes.STRING,
      loggedIn: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Session",
    }
  );
  return Session;
};
