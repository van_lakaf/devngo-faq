'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OTP extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     * @param {{ [member: string]: Model; }} models
     */
    static associate(models) {
      const { User } = models;
      User.hasMany(OTP, {
        foreignKey: "userId"
      });
      OTP.belongsTo(User);
    }
  }
  OTP.init({
    userId: DataTypes.INTEGER,
    otpValue: DataTypes.STRING,
    used: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'OTP',
  });
  return OTP;
};