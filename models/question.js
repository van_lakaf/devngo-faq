'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Question extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     * @param {{ [member: string]: Model; }} models
     */
    static associate(models) {
      const { User } = models;
      User.hasMany(Question, {
        foreignKey: "userId"
      });
      Question.belongsTo(User);
    }
  }
  Question.init({
    userId: DataTypes.INTEGER,
    questionValue: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Question',
  });
  return Question;
};