'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Response extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     * @param {{ [member: string]: Model; }} models
     */
    static associate(models) {
      const { Question, User } = models;
      User.hasMany(Response, {
        foreignKey: "userId"
      });
      Question.hasMany(Response, {
        foreignKey: "questionId"
      });
      Response.belongsTo(User);
      Response.belongsTo(Question);
    }
  }
  Response.init({
    userId: DataTypes.INTEGER,
    questionId: DataTypes.INTEGER,
    responseValue: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Response',
  });
  return Response;
};