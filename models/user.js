'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     * @param {{ [member: string]: Model; }} models
     */
    static associate(models) {
      const { Role } = models;
      Role.hasMany(User, {
        foreignKey: "roleId"
      });
      User.belongsTo(Role);
    }
  }
  User.init({
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    roleId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};