const { Router } = require("express");
const QuestionController = require("../../controllers/question.controller");
const ResponseController = require("../../controllers/response.controller");
const UserController = require("../../controllers/user.controller");

const authMiddleware = require("../../middlewares/auth.middleware");
const userIsAdminMiddleware = require("../../middlewares/user-is-admin.middleware");

const questionController = new QuestionController();
const responseController = new ResponseController();
const userController = new UserController();

const apiRoutes = Router({ mergeParams: true });

apiRoutes
  .post("/auth/sign-up", userController.signUp)
  .post("/auth/login/first-step", userController.findUser)
  .post("/auth/login/first-step/admin", userController.findUser)
  .post("/auth/login/second-step", userController.verifyUserOTP)
  .post("/auth/login/second-step/admin", userController.verifyUserOTP)
  .post("/auth/log-out", authMiddleware, userController.logOut)
  .get(
    "/questions",
    authMiddleware,
    userIsAdminMiddleware,
    questionController.index
  )
  .post("/questions", authMiddleware, questionController.store)
  .get(
    "/questions/:questionId",
    authMiddleware,
    userIsAdminMiddleware,
    questionController.show
  )
  .get(
    "/questions/:questionId/responses",
    authMiddleware,
    userIsAdminMiddleware,
    responseController.index
  )
  .post(
    "/questions/:questionId/responses",
    authMiddleware,
    userIsAdminMiddleware,
    responseController.store
  );

exports.apiRoutes = apiRoutes;
