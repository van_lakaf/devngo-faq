const { Router } = require("express");
const ViewService = require("../../services/view.service");

const webRoutes = Router({ mergeParams: true });
const viewService = new ViewService();

webRoutes
  .get("/", viewService.homeView)
  .get("/question/add", viewService.addQuestionView);

exports.webRoutes = webRoutes;
