"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return await queryInterface.bulkInsert("users", [
      {
        email: "kanguevan76@gmail.com",
        username: "Admin ADMIN",
        roleId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        email: "kanguevan1@gmail.com",
        username: "Vaneck KAFFO",
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return await queryInterface.bulkDelete("users", null, {});
  },
};
