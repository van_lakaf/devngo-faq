"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return await queryInterface.bulkInsert("roles", [
      {
        roleName: "admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        roleName: "visitor",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return await queryInterface.bulkDelete("roles", null, {});
  },
};
