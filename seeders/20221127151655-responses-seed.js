"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert(
      "responses",
      [
        {
          userId: 1,
          questionId: 1,
          responseValue: "Exemple de reponse pour la question 1...",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 1,
          questionId: 2,
          responseValue: "Exemple de reponse pour la question 2...",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    return await queryInterface.bulkDelete("responses", null, {});
  },
};
