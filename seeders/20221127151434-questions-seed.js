"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return await queryInterface.bulkInsert(
      "questions",
      [
        {
          userId: 1,
          questionValue: "Exemple de question sur la page 1...?",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 1,
          questionValue: "Exemple de question sur la page 2...?",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 1,
          questionValue: "Exemple de question sur la page 3...?",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    return await queryInterface.bulkDelete("questions", null, {});
  },
};
